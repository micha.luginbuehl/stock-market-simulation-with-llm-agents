import pymongo
import sys
import os
import simulation
import logging
import autogen
import pydantic
import time
import requests
import openai
import inspect
import memgpt

current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
sys.path.append(parent_dir)

from util import Config
from datetime import datetime, timedelta
from autogen import AssistantAgent, UserProxyAgent, GroupChat, GroupChatManager, config_list_from_json
from autogen.agentchat.contrib.capabilities import teachability
from autogen.agentchat.contrib.capabilities.teachability import Teachability
from simulation_classes import Simulation, SimulationDate
from data_classes import PolygonAggregates, FmpEconomicIndicators, FmpEsgScores
from memgpt.autogen.memgpt_agent import create_memgpt_autogen_agent_from_config, MemGPTAgent, load_autogen_memgpt_agent, MemGPTConversableAgent
from memgpt.constants import LLM_MAX_TOKENS
from memgpt.errors import LocalLLMError
from typing import Annotated, Callable
from autogen import OpenAIWrapper


# Configuration
c = Config()
config, current_path = c.config, c.path
# MongoDB
mongodb_client = pymongo.MongoClient(config["MONGO_DB"]["ClientUri"])
mongodb_db = mongodb_client[config["MONGO_DB"]["DbName"]]

CODE_EXECUTION_FOLDER = "autogen_workdir"
TEMPERATURE = 0.1
SEED = 231
DO_USE_LOCAL_LLM = False
LOCAL_LLM_SERVER = "masked"
LOCAL_LLM_PORT = 11434
DO_DEBUG = False
# Teachability
DO_ENABLE_TEACHABILITY = True
DO_RESET_TEACHABILITY_DB = True
TEACHABILITY_VERBOSITY = 0
TEACHABILITY_MAX_NUM_RETRIEVALS = 5
TECHABILITY_RECALL_THRESHOLD = 0.25

# Constants
PHASE_INSERT_RECOMMENDATION = simulation.PHASE_INSERT_RECOMMENDATION
MAX_RETRIES = 10

if DO_DEBUG:
    logging.basicConfig(level=logging.DEBUG)

config_list = config_list_from_json(env_or_file="OAI_CONFIG_LIST")

llm_config_openai = {
    "config_list": config_list,
    "seed": SEED,
    "max_retries": 10,
    "timeout": 30,
    "temperature": TEMPERATURE
}

termination_notice = (
    '\n\nDo not show appreciation in your responses, say only what is necessary. '
    'if "Thank you" or "You\'re welcome" are said in the conversation, then say TERMINATE '
    'to indicate the conversation is finished and this is your last message.'
)

""" Define User Proxies """
user_proxy = UserProxyAgent(
    name="user_proxy",
    system_message="You are the boss and execute functions if requested. Verify that all recommendations were made by the traders. Don't interrupt the traders if not necessary. If the traders get stuck tell them to use the functions 'get_stock_news' or 'get_technical_analysis'.",
    code_execution_config={"work_dir": CODE_EXECUTION_FOLDER, "use_docker": True},
    human_input_mode="NEVER",
    is_termination_msg=lambda x: x.get("content", "") and x.get("content", "").rstrip().endswith("TERMINATE"),
    llm_config=llm_config_openai,
)

user_proxy_pm = UserProxyAgent(
    name="user_proxy_pm",
    system_message="Support the Portfolio Manager in his investment decisions. Remind him to execute the functions 'pm_buy_ticker' for buying shares, 'pm_sell_ticker' for selling shares and 'pm_confirm_trading_completed' once he is finished.",
    code_execution_config={"work_dir": CODE_EXECUTION_FOLDER, "use_docker": True},
    human_input_mode="NEVER",
    is_termination_msg=lambda x: x.get("content", "") and x.get("content", "").rstrip().endswith("TERMINATE"),
    llm_config=llm_config_openai,
)



""" Traders """
# Instantiate the Teachability capability. Its parameters are all optional. Use GPT 3.5 for teachability
teachability_trader_1 = teachability.Teachability(
    verbosity=TEACHABILITY_VERBOSITY,  # 0 for basic info, 1 to add memory operations, 2 for analyzer messages, 3 for memo lists.
    reset_db=DO_RESET_TEACHABILITY_DB,
    path_to_db_dir=f"./{CODE_EXECUTION_FOLDER}/teachability_db/Trader1",
    recall_threshold=TECHABILITY_RECALL_THRESHOLD,  # Higher numbers allow more (but less relevant) memos to be recalled.
    max_num_retrievals=TEACHABILITY_MAX_NUM_RETRIEVALS,
    llm_config=llm_config_openai
)
teachability_trader_2 = teachability.Teachability(
    verbosity=TEACHABILITY_VERBOSITY,  # 0 for basic info, 1 to add memory operations, 2 for analyzer messages, 3 for memo lists.
    reset_db=DO_RESET_TEACHABILITY_DB,
    path_to_db_dir=f"./{CODE_EXECUTION_FOLDER}/teachability_db/Trader2",
    recall_threshold=TECHABILITY_RECALL_THRESHOLD,  # Higher numbers allow more (but less relevant) memos to be recalled.
    max_num_retrievals=TEACHABILITY_MAX_NUM_RETRIEVALS,
    llm_config=llm_config_openai
)
teachability_trader_3 = teachability.Teachability(
    verbosity=TEACHABILITY_VERBOSITY,  # 0 for basic info, 1 to add memory operations, 2 for analyzer messages, 3 for memo lists.
    reset_db=DO_RESET_TEACHABILITY_DB,
    path_to_db_dir=f"./{CODE_EXECUTION_FOLDER}/teachability_db/Trader3",
    recall_threshold=TECHABILITY_RECALL_THRESHOLD,  # Higher numbers allow more (but less relevant) memos to be recalled.
    max_num_retrievals=TEACHABILITY_MAX_NUM_RETRIEVALS,
    llm_config=llm_config_openai
)

def reply_trader(recipient: AssistantAgent, messages: list, sender: UserProxyAgent, config):
    last_message: str = messages[-1].get("content")
    if last_message == None:
        last_message = ""
    # Check if the current phase is completed
    if not "feedback for the current recommendation" in last_message and (
        (simulation.get_current_component_name() not in [PHASE_INSERT_RECOMMENDATION] and simulation.check_current_component_completed()) \
        or (simulation.get_current_component_name() == PHASE_INSERT_RECOMMENDATION and simulation.check_trader_recommendation_exists()) \
        or last_message == "TERMINATE"
    ):
        print("INFO: reply_trader ending criteria met. TERMINATING")
        return True, "TERMINATE"
    else:
        reply = recipient.generate_reply(messages=messages, sender=sender, exclude=[reply_trader])
        return True, reply


def create_autogen_trader(trader_name: str, system_prompt, teachability_db: Teachability, model: str = "mixtral:instruct") -> AssistantAgent:
    if DO_USE_LOCAL_LLM:
        config_list = [{
                "model": model,
                "base_url": f"http://{LOCAL_LLM_SERVER}:{LOCAL_LLM_PORT}/v1/",
                "api_key": "ollama"
            }]
    else:
        config_list = llm_config_openai.get("config_list")
    trader = AssistantAgent(
        name=trader_name,
        system_message=system_prompt,
        llm_config= {
            "config_list": config_list,
            "seed": SEED,
            "max_retries": 10,
            "timeout": 30,
            "temperature": TEMPERATURE
        },
        human_input_mode="NEVER",
        is_termination_msg=lambda x: x.get("content", "") and x.get("content", "").rstrip().endswith("TERMINATE"),
        max_consecutive_auto_reply=10,
        code_execution_config=False,
    )
    trader.register_reply(
        trigger=autogen.UserProxyAgent,
        reply_func=reply_trader
    )

    # Make agent teachable
    if DO_ENABLE_TEACHABILITY:
        teachability_db.add_to_agent(trader)
    return trader

# Create agents
trader_list = []
trader_settings_dict: dict = {
    "Trader1": {"teachability_db": teachability_trader_1, "model": None, "ins_recommendation_func": "insert_trader_recommendation_trader_1", "system_prompt": "Your name is Trader1. You need to insert action recommendations for stocks. You are a sentiment trader. News are very important for you and you try to identify and participate in trends."},
    "Trader2": {"teachability_db": teachability_trader_2, "model": None, "ins_recommendation_func": "insert_trader_recommendation_trader_2", "system_prompt": "Your name is Trader2. You need to insert action recommendations for stocks. You are a technical trader. You rely mostly on historical data and look for potential signs of divergence or convergence that may suggest good times to buy or sell a particular stock."},
    "Trader3": {"teachability_db": teachability_trader_3, "model": None, "ins_recommendation_func": "insert_trader_recommendation_trader_3", "system_prompt": "Your name is Trader3. You need to insert action recommendations for stocks. You are a fundamental trader. You analyze company-specific events to decide whether to buy or sell a particular stock."},
}
for key, value_dict in trader_settings_dict.items():
    trader: AssistantAgent = create_autogen_trader(
        trader_name=key,
        system_prompt=value_dict.get("system_prompt"),
        teachability_db=value_dict.get("teachability_db"),
        model=value_dict.get("model")        
    )
    trader_list.append(trader)



""" Portfolio Manager LLM """
teachability_portfolio_manager = teachability.Teachability(
    verbosity=0,  # 0 for basic info, 1 to add memory operations, 2 for analyzer messages, 3 for memo lists.
    reset_db=DO_RESET_TEACHABILITY_DB,
    path_to_db_dir=f"./{CODE_EXECUTION_FOLDER}/teachability_db/PortfolioManager",
    recall_threshold=TECHABILITY_RECALL_THRESHOLD,  # Higher numbers allow more (but less relevant) memos to be recalled.
)
# Generate a reply for PM
def reply_pm(recipient: MemGPTConversableAgent, messages: list, sender: UserProxyAgent, config):
    if simulation.is_pm_llm_finished():
        return True, "TERMINATE"        
    else:
        reply = recipient.generate_reply(messages=messages, sender=sender, exclude=[reply_pm])
        return True, reply

if DO_USE_LOCAL_LLM:
    config_list = [{
            "model": "dolphin-mixtral:latest", #"mixtral:instruct",
            "base_url": f"http://{LOCAL_LLM_SERVER}:{LOCAL_LLM_PORT}/v1/",
            "api_key": "ollama"
        }]
else:
    config_list = llm_config_openai.get("config_list")
portfolio_manager = AssistantAgent(
    name="PortfolioManager",
    system_message="Your name is PortfolioManager. You manage a portfolio and your goal is to make as much profit as possible by buying and selling stocks. Use function 'pm_buy_ticker' for buying shares, 'pm_sell_ticker' for selling shares and 'pm_confirm_trading_completed' if you don't want to perform any more trades.",
    llm_config= {
        "config_list": config_list,
        "seed": SEED,
        "max_retries": 5,
        "timeout": 30,
        "temperature": TEMPERATURE
    },
    human_input_mode="NEVER",
    is_termination_msg=lambda x: x.get("content", "") and x.get("content", "").rstrip().endswith("TERMINATE"),
    max_consecutive_auto_reply=10,
    code_execution_config=False,
    )

portfolio_manager.register_reply(
    trigger=autogen.UserProxyAgent,
    reply_func=reply_pm
)
if DO_ENABLE_TEACHABILITY:
    teachability_portfolio_manager.add_to_agent(portfolio_manager)


# Wrapper to initiate chat because of exception handling
def initiate_chat(initiator: UserProxyAgent, recipient: AssistantAgent, message: str, clear_history: bool = True, func_call_filter: bool = True, max_turns: int = None):

    def make_request():
        initiator.initiate_chat(recipient=recipient,
                                message=message,
                                clear_history=clear_history,
                                func_call_filter=func_call_filter,
                                request_reply=True,
                                max_turns=max_turns)
        
    def log_err(err_text):
        logging.exception(err_text)
        print(err_text)
    
    retries = 5
    for attempt in range(retries):
        try:
            make_request()
            break  # Success, no need to retry
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 500:  # Retry only for 500 Internal Server Error (OpenAI side problems)
                log_err("500 Error. Retrying in 5 seconds...")
                time.sleep(5)
            elif err.response.status_code == 400: # Bad Request for url
                log_err("EXCEPTION: 400 Client Error: Bad Request for url: https://api.openai.com/v1/chat/completions. Deleting history...")
                print(f"{initiator.name=}, {recipient.name=}")
                initiator.clear_history()
                recipient.clear_history()
            else:
                # For other HTTP errors, raise the exception again
                log_err(f"EXCEPTION: {err.response.status_code}")
                raise
        except requests.exceptions.Timeout:
            log_err("Request timed out")
            raise  # Reraise the exception
        # Failed to parse JSON from local LLM response - error: Failed to decode JSON from LLM output
        except LocalLLMError:
            log_err("memgpt_backup.errors.LocalLLMError")
            time.sleep(1)
        except Exception as e:
            exception_type = type(e).__name__  # Retrieve the type of the exception as a string
            error_message = str(e)  # Retrieve the error message
            log_err(f"An unexpected error occurred: {exception_type}: {error_message}")
            if exception_type == "BadRequestError":
                # Local LLM's tend to malform function calls, this is usually unrecoverable
                if DO_USE_LOCAL_LLM and "invalid role: tool, role must be one of [system, user, assistant]" in error_message:
                    sys.exit()
                else:
                    initiator.clear_history()
                    recipient.clear_history()
            else:
                raise  # Reraise the exception
    else:
        log_err("Failed to complete request after retries")


""" API functions Traders """
# submit_component_score
function_name="submit_component_score"
function_description="Insert a component score for the current component. The score must be between -1 (very negative) and 1 (very positive). This function needs to be called once for each ticker and component."  
@user_proxy.register_for_execution()
@user_proxy.register_for_llm(description=function_description, api_style="tool")
def submit_component_score(score: Annotated[float, "Component score, must be between -1 (very negative) and 1 (very positive)"], explanation: Annotated[str, "Explanation why this score was chosen"]) -> Annotated[str, "Feedback if the submission was successful"]:
    return simulation.submit_component_score(score=score, explanation=explanation)
for trader in trader_list:    
    trader.register_for_execution() (submit_component_score)
    trader.register_for_llm(description=function_description, api_style="tool") (submit_component_score)

# insert_trader_recommendation_trader_1
function_name="insert_trader_recommendation_trader_1"
function_description="Insert a recommended action for a symbol or ticker for the current timestep. Call this function only when you are explicitely asked to. All recommendation components have to be submitted first."   
@user_proxy.register_for_execution()
@user_proxy.register_for_llm(description=function_description, api_style="tool")
def insert_trader_recommendation_trader_1(symbol: Annotated[str, "Stock symbol or ticker"], recommended_action: Annotated[str, "Recommended action. Allowed values are 'long' (hold a long position), 'short' (hold a short position) or 'neutral' (do not hold any position)"], explanation: Annotated[str, "Explanation why the recommended action has been chosen"] = None) -> Annotated[str, "Feedback if insertion was succesful and if the recommendation was good or bad"]:
    return simulation.insert_trader_recommendation_trader_1(symbol=symbol, recommended_action=recommended_action, explanation=explanation)
for trader in trader_list:
    if trader.name == "Trader1":    
        trader.register_for_execution() (insert_trader_recommendation_trader_1)
        trader.register_for_llm(description=function_description, api_style="tool") (insert_trader_recommendation_trader_1)

# insert_trader_recommendation_trader_2
function_name="insert_trader_recommendation_trader_2"
function_description="Insert a recommended action for a symbol or ticker for the current timestep. Call this function only when you are explicitely asked to. All recommendation components have to be submitted first."   
@user_proxy.register_for_execution()
@user_proxy.register_for_llm(description=function_description, api_style="tool")
def insert_trader_recommendation_trader_2(symbol: Annotated[str, "Stock symbol or ticker"], recommended_action: Annotated[str, "Recommended action. Allowed values are 'long' (hold a long position), 'short' (hold a short position) or 'neutral' (do not hold any position)"], explanation: Annotated[str, "Explanation why the recommended action has been chosen"] = None) -> Annotated[str, "Feedback if insertion was succesful and if the recommendation was good or bad"]:
    return simulation.insert_trader_recommendation_trader_2(symbol=symbol, recommended_action=recommended_action, explanation=explanation)
for trader in trader_list:
    if trader.name == "Trader2":    
        trader.register_for_execution() (insert_trader_recommendation_trader_2)
        trader.register_for_llm(description=function_description, api_style="tool") (insert_trader_recommendation_trader_2)

# insert_trader_recommendation_trader_3
function_name="insert_trader_recommendation_trader_3"
function_description="Insert a recommended action for a symbol or ticker for the current timestep. Call this function only when you are explicitely asked to. All recommendation components have to be submitted first."   
@user_proxy.register_for_execution()
@user_proxy.register_for_llm(description=function_description, api_style="tool")
def insert_trader_recommendation_trader_3(symbol: Annotated[str, "Stock symbol or ticker"], recommended_action: Annotated[str, "Recommended action. Allowed values are 'long' (hold a long position), 'short' (hold a short position) or 'neutral' (do not hold any position)"], explanation: Annotated[str, "Explanation why the recommended action has been chosen"] = None) -> Annotated[str, "Feedback if insertion was succesful and if the recommendation was good or bad"]:
    return simulation.insert_trader_recommendation_trader_3(symbol=symbol, recommended_action=recommended_action, explanation=explanation)
for trader in trader_list:
    if trader.name == "Trader3":    
        trader.register_for_execution() (insert_trader_recommendation_trader_3)
        trader.register_for_llm(description=function_description, api_style="tool") (insert_trader_recommendation_trader_3) 


""" API functions PM """
# pm_buy_ticker
function_name="pm_buy_ticker"
function_description="Buy a ticker or symbol. The ticker will be added to your portfolio if you have got enough cash."
@user_proxy_pm.register_for_execution()
@user_proxy_pm.register_for_llm(description=function_description, api_style="tool")
@portfolio_manager.register_for_execution()
@portfolio_manager.register_for_llm(description=function_description, api_style="tool")
def pm_buy_ticker(ticker: Annotated[str, "Stock symbol or ticker you want to buy"], quantity: Annotated[int, "Quantity of stocks that you want to buy"], explanation: Annotated[str, "Explanation why you want to buy the the ticker"] = None) -> Annotated[str, "Feedback if the ticker was successfully bought"]:
    return simulation.buy_ticker(ticker=ticker, portfolio_manager=simulation.PM_LLM, quantity=quantity, explanation=explanation)

# pm_sell_ticker
function_name="pm_sell_ticker"
function_description="Buy a ticker or symbol. The ticker will be added to your portfolio if you have got enough cash."
@user_proxy_pm.register_for_execution()
@user_proxy_pm.register_for_llm(description=function_description, api_style="tool")
@portfolio_manager.register_for_execution()
@portfolio_manager.register_for_llm(description=function_description, api_style="tool")
def pm_sell_ticker(ticker: Annotated[str, "Stock symbol or ticker you want to buy"], quantity: Annotated[int, "Quantity of stocks that you want to sell"], explanation: Annotated[str, "Explanation why you want to sell the the ticker"] = None) -> Annotated[str, "Feedback if the ticker was successfully sold"]:
    return simulation.sell_ticker(ticker=ticker, portfolio_manager=simulation.PM_LLM, quantity=quantity, explanation=explanation)

# pm_confirm_trading_completed
function_name="pm_confirm_trading_completed"
function_description="If you do not want to buy or sell any more shares this timestep call this function to confirm. This will end the trading phase."
@user_proxy_pm.register_for_execution()
@user_proxy_pm.register_for_llm(description=function_description, api_style="tool")
@portfolio_manager.register_for_execution()
@portfolio_manager.register_for_llm(description=function_description, api_style="tool")
def pm_confirm_trading_completed() -> Annotated[str, "Confirmation that trades are no longer possible for this timestep"]:
    return simulation.confirm_pm_llm_trading_completed()

# pm_buy_ticker
function_name="get_portfolio_content"
function_description="Get the current content of your portfolio."
@user_proxy_pm.register_for_execution()
@user_proxy_pm.register_for_llm(description=function_description, api_style="tool")
@portfolio_manager.register_for_execution()
@portfolio_manager.register_for_llm(description=function_description, api_style="tool")
def get_portfolio_content() -> Annotated[str, "An overview over the current cash and positions held in your portfolio"]:
    portfolio_content, _ = simulation.get_portfolio_content(portfolio_manager=simulation.PM_LLM)
    return portfolio_content

""" Start a simulation """
do_generate_new_simulation = False
trader_dict = {trader.name: trader for trader in trader_list}
pm_list = [simulation.PM_LLM, simulation.PM_ALGO]
if do_generate_new_simulation:
    start_date = datetime(2021, 12, 31, 9, 0)
    end_date = datetime(2024, 1, 2, 9, 0)
    tickers_list = ["AAPL", "BA", "WMT", "NKE", "JPM"]
    initial_cash_amount = 100e3
    simulation_id = simulation.generate_simulation(
        start_date=start_date,
        end_date=end_date,
        tickers=tickers_list,
        trader_list=[trader.name for trader in trader_list],
        cash_amount=initial_cash_amount,
        cost_percentage=0.001,
    )
components = {
    "General news":         {"input_func": simulation.get_general_news,         "instruct_prompt": f"Follow the instructions carefully. Analyze the general news articles provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the articles indicate good or bad future performance in short term. Consider that some news articles might be more relevant than others in your score."},
    "Stock news":           {"input_func": simulation.get_stock_news,           "instruct_prompt": f"Follow the instructions carefully. Analyze the stock news articles provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the articles indicate good or bad future performance in short term. Consider that some news articles might be more relevant than others in your score."},
    "Prices and volumes":   {"input_func": simulation.get_last_stock_prices,    "instruct_prompt": f"Follow the instructions carefully. Analyze the last prices provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the data indicates good or bad future performance in the short term."},
    "Technical analysis":   {"input_func": simulation.get_technical_analysis,   "instruct_prompt": f"Follow the instructions carefully. Analyze the technical data provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the data indicates good or bad future performance in the short term."},
    "Financials":           {"input_func": simulation.get_financials,           "instruct_prompt": f"Follow the instructions carefully. Analyze the provided Management discussion and analysis from the latest published SEC report and the financial fundamental KPI's provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the data indicates good or bad future performance in the short term."},
    "Economic indicators":  {"input_func": simulation.get_economic_indicators,  "instruct_prompt": f"Follow the instructions carefully. Analyze the economic indicators provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the data indicates good or bad future performance in the short term."},
    "Miscellaneous":        {"input_func": simulation.get_misc_info,            "instruct_prompt": f"Follow the instructions carefully. Analyze the KPI's provided under the tag ###DATA### using only your own capabilities (no function call required) for ##TICKER## and define a score if the data indicates good or bad future performance in the short term."}    
}

original_stdout = sys.stdout
while (current_date := simulation.get_current_simulation().current_date) < simulation.get_current_simulation().end_date:
    start_time: float = time.perf_counter()     
    logfile_path = f"agent_chat\chat_logs\{simulation.get_current_simulation().simulation_name}\{simulation.get_current_simulation().current_date.strftime('%Y-%m-%d')}.log"
    with open(file=logfile_path, encoding="UTF-8", mode="w") as logfile:
        sys.stdout = logfile

        for trader in trader_list:
            for ticker in simulation.get_current_simulation().tickers_list:

# Phase 1: Get scores for all recommendation components
                # Register components
                for idx, component in enumerate(components):
                    simulation.create_recommendation_component(component_name=component, agent_name=trader.name, ticker=ticker, seq_nr=idx)
                last_component = ""
                retries = 0
                while (component_document := simulation.get_next_component(agent_name=trader.name, ticker=ticker)):
                    component_name = component_document.name
                    if component_name != last_component:
                        retries = 0
                    else:
                        retries += 1
                    if retries > MAX_RETRIES:
                        print(f"ERROR: agent exceeded maximum of {MAX_RETRIES}. Submitting neutral score")
                        simulation.submit_component_score(score=0, explanation=f"ERROR: agent exceeded maximum of {MAX_RETRIES} retries. Submitting neutral score")
                        continue    
                    simulation.set_current_component(agent_name=trader.name, ticker=ticker, component_name=component_name)        
                    component_dict = components.get(component_name)
                    ret_func: Callable[..., str] = component_dict["input_func"]
                    if "symbol" in inspect.signature(ret_func).parameters:
                        ret_func_str = str(ret_func(symbol=ticker))
                    else:
                        ret_func_str = str(ret_func())
                    instruct_prompt: str = component_dict['instruct_prompt'].replace('##TICKER##', ticker)
                    trader.update_system_message(f"{trader_settings_dict.get(trader.name)['system_prompt']}. Do not suggest Python code, use only the available function calls for code execution. {instruct_prompt}")
                    user_proxy.update_system_message("You are the boss and remind the traders that they need to submit their scores component scores as soon as possible using tool call 'submit_component_score'. Don't interrupt the traders if not necessary and don't do analysis for them.")
                    # Prepare message
                    message = f"""### INSTRUCTIONS ###
The current timestep is {current_date.strftime('%d.%m.%Y')}. The component has changed. We're currently analyzing the component '{component_name}' for ticker '{ticker}'. Follow these instructions carefully.
1. {instruct_prompt}. The score must be a number with a value between -1 (very negative) and 1 (very positive), 0 is neutral.
2. You have to submit your component score using the appropriate tool call 'submit_component_score'. Do not provide custom code to execute.
Explain step by step your reasoning. Important: do not insert a trader recommendation yet, only analyze this component and submit the score.
### DATA ###
{ret_func_str}"""                   

                    simulation.set_last_instruction(message)
                    initiate_chat(
                        initiator=user_proxy,
                        recipient=trader,
                        message=message,
                        clear_history=True, # just for cost and performance reasons - False would be better though
                        func_call_filter=True,
                        max_turns=10
                    )
                    last_component = component_name

# Phase 2: Insert trader recommendation and receive instant feedback
                simulation.set_current_component(agent_name=trader.name, ticker=ticker, component_name=PHASE_INSERT_RECOMMENDATION)
                while not simulation.check_trader_recommendation_exists():  
                    trader.update_system_message(f"Do not suggest Python code, use only the available function calls for code exceution. {trader_settings_dict.get(trader.name)['system_prompt']}")
                    user_proxy.clear_history() # Clear history for cost reasons (runs on OpenAI)
                    user_proxy.update_system_message(f"You are the boss and support the traders. Remind the trader {trader.name} to insert his recommendation for ticker {ticker} using function '{trader_settings_dict.get(trader.name)['ins_recommendation_func']}'. Don't interrupt the traders if not necessary.")    
                    message = f"""### INSTRUCTIONS ### 
Follow these instructions very carefully! Under the tag ### COMPONENTS ### you see your components score for the next recommendation for ticker {ticker}. Your last recommendation was '{simulation.get_last_trader_recommendation(agent_name=trader.name, ticker=ticker)}'.
1. Analyze your scores and take into consideration the feedback under the tag ### FEEDBACK ### to make a final trader recommendation for ticker {ticker}. Choose the recommended action for the next timestep. The  possible actions are 'long' (hold a long position), 'short' (hold a short position) or 'neutral' (do not hold any position)
2. Submit your recommendation using function '{trader_settings_dict.get(trader.name)["ins_recommendation_func"]}'.
Explain step by step your reasoning. Also consider the distribution of the ground truth by analyzing the confusion matrix and your current position ('{simulation.get_last_trader_recommendation(agent_name=trader.name, ticker=ticker)}') in your recommendation. Each change in position has transaction costs of {simulation.get_current_simulation().cost_percentage * 100}%.
### COMPONENTS ###
{simulation.get_recommendation_components(agent_name=trader.name, ticker=ticker)}
### FEEDBACK ###
{simulation.get_detailed_feedback_for_stock(agent_name=trader.name, symbol=ticker)}
"""
                    simulation.set_last_instruction(message)                
                    initiate_chat(
                        initiator=user_proxy,
                        recipient=trader,
                        message=message,
                        clear_history=True,
                        func_call_filter=True,
                        max_turns=5
                    )            


# Phase 3: Portfolio Managers
        # Disable simulation for first 30 days to accumulate some information about traders performance first
        if current_date > simulation.get_current_simulation().start_date + timedelta(days=30):
            # Pay out dividends
            for pm in pm_list:        
                simulation.pay_out_dividends(portfolio_manager=pm)
                simulation.register_dividend_payouts(portfolio_manager=pm)

            # Portfolio Manager LLM
            user_proxy_pm.clear_history()
            portfolio_manager.clear_history()
            while not simulation.is_pm_llm_finished():
                current_portoflio, _ = simulation.get_portfolio_content(portfolio_manager=simulation.PM_LLM)
                initiate_chat(
                    initiator=user_proxy_pm,
                    recipient=portfolio_manager,
                    message=
f"""Your current portfolio includes:
{current_portoflio}
{simulation.get_trader_recommendation_overview()}
# INSTRUCTIONS
1. Trade execution
- Buy or sell shares based on the recommendations provided.
- Follow the recommendations carefully.
- Do not buy any stock you just sold or sell any stock you just bought.
- Don't invest everything in a single stock. 
2. Reasoning:
- Explain the reasoning behind each trade.
3. Trading costs:
- Each trade incurs a cost of {simulation.get_current_simulation().cost_percentage * 100}%.
4. Objective
- The goal is to maximize your profit.
- Ensure that investments are distributed more or less evenly across different stocks, rather than concentrating on a single stock.
5. Completion
- If you decide not to trade any more shares, call the function 'pm_confirm_trading_completed' to complete your trading session.""",
                    clear_history=True,
                    func_call_filter=True,
                    max_turns=30
                )

            # Give feedback to LLM Portfolio Manager
            user_proxy_pm.send(
                recipient=portfolio_manager,
                message=simulation.get_feedback_pm_llm(),
                request_reply=False
            )

            # Portfolio Manager Algo
            simulation.algo_pm_perform_trading()  
        
        # Print Trader overall performance (this is after feedback for current day)
        print(simulation.get_trader_overall_performance())

        # Switch simulation day    
        end_time: float = time.perf_counter()
        elapsed_time: float = end_time - start_time     
        sys.stdout = original_stdout
        simulation.switch_simulation_day(chat_log_path=logfile_path, elapsed_time=elapsed_time)