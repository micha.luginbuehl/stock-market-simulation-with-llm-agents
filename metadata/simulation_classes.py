from mongoengine import connect, Document, DateTimeField, StringField, BooleanField, FloatField, ListField, IntField, EmbeddedDocumentField, EmbeddedDocument
from datetime import datetime, timedelta
from pydantic_core import CoreSchema, core_schema
from pydantic import GetCoreSchemaHandler, TypeAdapter
from bson import ObjectId
from typing import Any

simulation_db_alias = "simulation_db"

""" Simulation DB Documents """
# Document Definitions
class Simulation(Document):
    simulation_id = StringField(required=True)
    simulation_name = StringField(required=True)
    start_date = DateTimeField(required=True)
    end_date = DateTimeField(required=True)
    current_date = DateTimeField(required=True)
    tickers_list = ListField(StringField(), required=True)
    timestamp = DateTimeField(default=datetime.utcnow)
    is_running = BooleanField(required=True, default=False)
    trader_list = ListField(StringField(), required=True)
    cost_percentage= FloatField()
    initial_cash_amount = FloatField()
    current_component_agent = StringField()
    current_component_ticker = StringField()
    current_component_name = StringField()
    last_instruction = StringField()

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [
            {"fields": ["is_running"]},
            {"fields": ["simulation_id"], "unique": True},
        ]
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str)) 


class AccuracyList(EmbeddedDocument):
    agent_name = StringField()
    accuracy = FloatField()


class TraderRecommendations(Document):
    simulation_id = StringField(required=True)
    date = DateTimeField(required=True)
    ticker = StringField(required=True)
    agent_name = StringField(required=True)
    recommended_action = StringField(required=True, choices=["long", "short", "neutral"])
    explanation = StringField()
    current_price = FloatField()
    next_price = FloatField()
    current_trading_signal = IntField()
    previous_trading_signal = IntField()
    rel_change = FloatField()
    best_action = StringField()
    feedback = StringField()     
    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "date"]}]
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))

class Trade(Document):
    simulation_id = StringField(required=True)
    date = DateTimeField(required=True)
    ticker = StringField(required=True)
    quantity = FloatField(required=True)
    price = FloatField(required=True)
    costs = FloatField(required=True)
    gross_amount = FloatField(required=True)
    net_amount = FloatField(required=True)
    action = StringField(required=True, choices=["buy", "sell"])
    timestamp = DateTimeField(default=datetime.utcnow)
    explanation = StringField()
    portfolio_manager = StringField(required=True, choices=["pm_llm", "pm_algo"])

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "portfolio_manager"]}]
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))

class Position(EmbeddedDocument):
    simulation_id = StringField(required=True)
    ticker = StringField(required=True)
    quantity = FloatField(required=True, default=0.0)
    timestamp = DateTimeField(default=datetime.utcnow)
    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id"]}]
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    
class Portfolio(Document):
    simulation_id = StringField(required=True)
    cash = FloatField(required=True)
    portfolio_manager = StringField(required=True, choices=["pm_llm", "pm_algo"])
    positions = ListField(EmbeddedDocumentField(Position))
    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "portfolio_manager"]}]
    }
    
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class DividendPayouts(Document):
    simulation_id = StringField(required=True)
    ticker = StringField(required=True)
    amount_per_piece = FloatField(required=True)
    quantity = FloatField(required=True)
    total_amount = FloatField(required=True)
    pay_date = DateTimeField(required=True)
    paid = BooleanField(required=True)
    portfolio_manager = StringField(required=True, choices=["pm_llm", "pm_algo"])

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "portfolio_manager"]}]
    }
    
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class FunctionCallLogs(Document):
    simulation_id = StringField(required=True)
    simulation_date = DateTimeField(required=True)
    agent_name = StringField(required=True)
    function_name = StringField(required=True)
    function_args = StringField()
    symbol = StringField()
    timestamp = DateTimeField(default=datetime.utcnow)

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "simulation_date", "agent_name"]}]
    }
    
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class SimulationDate(Document):
    simulation_id = StringField(required=True)
    date = DateTimeField(required=True)
    timestamp = DateTimeField(default=datetime.utcnow)
    pm_llm_eod_positions = ListField(EmbeddedDocumentField(Position))
    pm_llm_eod_cash = FloatField()
    pm_llm_eod_total_val = FloatField()
    pm_algo_eod_positions = ListField(EmbeddedDocumentField(Position))
    pm_algo_eod_cash = FloatField()
    pm_algo_eod_total_val = FloatField()
    chat_log_path = StringField()
    accuracy_list = ListField(EmbeddedDocumentField(AccuracyList))
    pm_llm_completed = BooleanField(required=True)

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id"]}]
    }
    
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class RecommendationComponent(Document):
    simulation_id = StringField(required=True)
    seq_nr = IntField(required=True)
    date = DateTimeField(required=True)
    timestamp = DateTimeField(default=datetime.utcnow)
    name = StringField(required=True)
    ticker = StringField(required=True)
    agent_name = StringField(required=True)
    score = FloatField()
    explanation = StringField()    

    meta = {
        "db_alias": simulation_db_alias,
        "indexes": [{"fields": ["simulation_id", "date", "name", "agent_name"]}]
    }
    
    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))      