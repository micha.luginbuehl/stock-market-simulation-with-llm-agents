from mongoengine import connect, Document, DateTimeField, StringField, BooleanField, IntField, FloatField, EmbeddedDocument, EmbeddedDocumentField, ListField
from pydantic_core import CoreSchema, core_schema
from pydantic import GetCoreSchemaHandler, TypeAdapter
from typing import Any

data_db_alias = "data_db"

""" Data DB Documents"""
class PolygonAggregates(Document):
    ticker = StringField()
    volume = IntField()
    low = FloatField()
    high = FloatField()
    open = FloatField()
    close = FloatField()
    vwap = FloatField()
    transactions = IntField()
    timestamp = DateTimeField()
    otc = BooleanField()
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["ticker"]},
            {"fields": ["timestamp"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str)) 

class FmpEconomicIndicators(Document):
    date = DateTimeField()
    indicator = StringField()
    value = FloatField()
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date"]},
            {"fields": ["indicator"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str)) 

class FmpEsgScores(Document):
    date = DateTimeField()
    accepted_date = DateTimeField(db_field="acceptedDate")
    environmental_score = FloatField(db_field="environmentalScore")
    cik = StringField()
    symbol = StringField()
    form_type = StringField(db_field="formType")
    governance_score = FloatField(db_field="governanceScore")
    esg_score = FloatField(db_field="ESGScore")
    social_score = FloatField(db_field="socialScore")
    url = StringField()
    company_name = StringField(db_field="companyName")
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date"]},
            {"fields": ["symbol"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))

class FmpStockNews(Document):
    publishedDate = DateTimeField()
    symbol = StringField()
    title = StringField()
    text = StringField()
    image = StringField()
    site = StringField()
    url = StringField()
    id = StringField(db_field="_id")
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["publishedDate"]},
            {"fields": ["symbol"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))

class FmpPressReleases(Document):
    date = DateTimeField()
    symbol = StringField()
    title = StringField()
    text = StringField()
    id = StringField(db_field="_id")
    sentiment_score = FloatField(db_field="sentiment_score")
    simulation_date = DateTimeField()
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date"]},
            {"fields": ["symbol"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class FmpPriceTargets(Document):
    published_date = DateTimeField(db_field="publishedDate")
    symbol = StringField()
    price_target = FloatField(db_field="priceTarget")
    price_when_posted = FloatField(db_field="priceWhenPosted")
    analyst_name = StringField(db_field="analystName")
    news_publisher = StringField(db_field="newsPublisher")
    news_url = StringField(db_field="newsURL")
    adj_price_target = FloatField(db_field="adjPriceTarget")
    news_title = StringField(db_field="newsTitle")
    news_base_url = StringField(db_field="newsBaseURL")
    analyst_company = StringField(db_field="analystCompany")
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["published_date"]},
            {"fields": ["symbol"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class FmpHistRatings(Document):
    date = DateTimeField()
    symbol = StringField()
    rating = StringField()
    rating_score = IntField(db_field="ratingScore")
    rating_recommendation = StringField(db_field="ratingRecommendation")
    rating_details_dcf_score = IntField(db_field="ratingDetailsDCFScore")
    rating_details_dcf_recommendation = StringField(db_field="ratingDetailsDCFRecommendation")
    rating_details_roe_score = IntField(db_field="ratingDetailsROEScore")
    rating_details_roe_recommendation = StringField(db_field="ratingDetailsROERecommendation")
    rating_details_roa_score = IntField(db_field="ratingDetailsROAScore")
    rating_details_roa_recommendation = StringField(db_field="ratingDetailsROARecommendation")
    rating_details_de_score = IntField(db_field="ratingDetailsDEScore")
    rating_details_de_recommendation = StringField(db_field="ratingDetailsDERecommendation")
    rating_details_pe_score = IntField(db_field="ratingDetailsPEScore")
    rating_details_pe_recommendation = StringField(db_field="ratingDetailsPERecommendation")
    rating_details_pb_score = IntField(db_field="ratingDetailsPBScore")
    rating_details_pb_recommendation = StringField(db_field="ratingDetailsPBRecommendation")
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date"]},
            {"fields": ["symbol"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class PolygonTechAnalysisSMA(Document):
    timestamp = DateTimeField(required=True)
    ticker = StringField(required=True)
    value = FloatField(required=True)
    window = IntField(required=True)
    meta = {
        "db_alias": data_db_alias,
        "collection": "polygon_ta_sma",
        "indexes": [
            {"fields": ["timestamp", "ticker", "window"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class PolygonNews(Document):
    published_utc = DateTimeField(required=True)
    author = StringField()
    image_url = StringField()
    amp_url = StringField()
    keywords = ListField(StringField())
    title = StringField()
    publisher_name = StringField()
    ticker = StringField(required=True)
    article_url = StringField()
    description = StringField()
    id = StringField(db_field="_id")
    retrieved_article_content = StringField()
    sentiment_score = FloatField()
    simulation_date = DateTimeField()
    meta = {
        "db_alias": data_db_alias,
        "collection": "polygon_news",
        "indexes": [
            {"fields": ["published_utc", "ticker", "publisher_name", "article_url"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class PolygonTechAnalysisEMA(Document):
    timestamp = DateTimeField(required=True)
    ticker = StringField(required=True)
    value = FloatField(required=True)
    window = IntField(required=True)
    meta = {
        "db_alias": data_db_alias,
        "collection": "polygon_ta_ema",
        "indexes": [
            {"fields": ["timestamp", "ticker", "window"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class PolygonTechAnalysisRSI(Document):
    timestamp = DateTimeField(required=True)
    ticker = StringField(required=True)
    value = FloatField(required=True)
    meta = {
        "db_alias": data_db_alias,
        "collection": "polygon_ta_rsi",
        "indexes": [
            {"fields": ["timestamp"]},
            {"fields": ["ticker"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class PolygonTechAnalysisMACD(Document):
    timestamp = DateTimeField(required=True)
    ticker = StringField(required=True)
    signal = FloatField(required=True)
    histogram = FloatField(required=True)
    value = FloatField(required=True)
    meta = {
        "db_alias": data_db_alias,
        "collection": "polygon_ta_macd",
        "indexes": [
            {"fields": ["timestamp"]},
            {"fields": ["ticker"]},
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class NyTimesNews(Document):
    pub_date = DateTimeField(required=True)
    lead_paragraph = StringField()
    web_url = StringField()
    word_count = IntField()
    abstract = StringField()
    source = StringField()
    snippet = StringField()
    document_type = StringField()
    section_name = StringField()
    sentiment_score = FloatField()
    simulation_date = DateTimeField()
    id = StringField(db_field="_id")   
    article_id = StringField()
    meta = {
        "db_alias": data_db_alias,
        "collection": "nytimes_news",
        "indexes": [
            {"fields": ["pub_date"]},
            {"fields": ["section_name"]},
            {"fields": ["simulation_date"]}
        ]       
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class GeneralNewsSummarized(Document):
    date = DateTimeField(required=True)
    news_summary = StringField(required=True)
    sentiment_score = FloatField(db_field="sentiment_score")
    id = StringField(db_field="_id")
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date"]},
        ]    
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))
    

class StockNewsSummarized(Document):
    date = DateTimeField(required=True)
    ticker = StringField(required=True)
    news_summary = StringField(required=True)
    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["date", "ticker"]},
        ]    
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class PolygonDividends(Document):
    ex_dividend_date = DateTimeField(required=True)
    ticker = StringField(required=True)
    dividend_type = StringField()
    frequency = IntField()
    dividend_type = StringField()
    record_date_str = StringField(db_field="record_date")
    cash_amount = FloatField()
    declaration_date_str = StringField(db_field="declaration_date")
    currency = StringField()
    pay_date_str = StringField(db_field="pay_date")

    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["ex_dividend_date", "ticker"]},
        ]    
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))


class FinancialDataPoint(EmbeddedDocument):
    formula = StringField()
    label = StringField()
    order = IntField()
    unit = StringField()
    value = FloatField()
    xpath = StringField()


class BalanceSheet(EmbeddedDocument):
    other_noncurrent_liabilities = EmbeddedDocumentField(FinancialDataPoint)
    long_term_debt = EmbeddedDocumentField(FinancialDataPoint)
    noncurrent_assets = EmbeddedDocumentField(FinancialDataPoint)
    current_liabilities = EmbeddedDocumentField(FinancialDataPoint)
    noncurrent_liabilities = EmbeddedDocumentField(FinancialDataPoint)
    current_assets = EmbeddedDocumentField(FinancialDataPoint)
    assets = EmbeddedDocumentField(FinancialDataPoint)
    temporary_equity_attributable_to_parent = EmbeddedDocumentField(FinancialDataPoint)
    liabilities_and_equity = EmbeddedDocumentField(FinancialDataPoint)
    other_current_liabilities = EmbeddedDocumentField(FinancialDataPoint)
    other_noncurrent_assets = EmbeddedDocumentField(FinancialDataPoint)
    fixed_assets = EmbeddedDocumentField(FinancialDataPoint)
    accounts_payable = EmbeddedDocumentField(FinancialDataPoint)
    liabilities = EmbeddedDocumentField(FinancialDataPoint)
    temporary_equity = EmbeddedDocumentField(FinancialDataPoint)
    equity = EmbeddedDocumentField(FinancialDataPoint)
    equity_attributable_to_parent = EmbeddedDocumentField(FinancialDataPoint)
    equity_attributable_to_noncontrolling_interest = EmbeddedDocumentField(FinancialDataPoint)
    intangible_assets = EmbeddedDocumentField(FinancialDataPoint)
    wages = EmbeddedDocumentField(FinancialDataPoint)
    inventory = EmbeddedDocumentField(FinancialDataPoint)
    prepaid_expenses = EmbeddedDocumentField(FinancialDataPoint)
    other_current_assets = EmbeddedDocumentField(FinancialDataPoint)
    accounts_receivable = EmbeddedDocumentField(FinancialDataPoint)
    cash = EmbeddedDocumentField(FinancialDataPoint)
    long_term_investments = EmbeddedDocumentField(FinancialDataPoint)
    redeemable_noncontrolling_interest = EmbeddedDocumentField(FinancialDataPoint)
    redeemable_noncontrolling_interest_common = EmbeddedDocumentField(FinancialDataPoint)


class CashFlowStatement(EmbeddedDocument):
    exchange_gains_losses = EmbeddedDocumentField(FinancialDataPoint)
    net_cash_flow = EmbeddedDocumentField(FinancialDataPoint)
    net_cash_flow_from_financing_activities = EmbeddedDocumentField(FinancialDataPoint)


class ComprehensiveIncome(EmbeddedDocument):
    comprehensive_income_loss = EmbeddedDocumentField(FinancialDataPoint)
    comprehensive_income_loss_attributable_to_parent = EmbeddedDocumentField(FinancialDataPoint)
    other_comprehensive_income_loss = EmbeddedDocumentField(FinancialDataPoint)


class IncomeStatement(EmbeddedDocument):
    basic_earnings_per_share = EmbeddedDocumentField(FinancialDataPoint)
    cost_of_revenue = EmbeddedDocumentField(FinancialDataPoint)
    gross_profit = EmbeddedDocumentField(FinancialDataPoint)
    operating_expenses = EmbeddedDocumentField(FinancialDataPoint)
    revenues = EmbeddedDocumentField(FinancialDataPoint)


class Financials(EmbeddedDocument):
    balance_sheet = EmbeddedDocumentField(BalanceSheet)
    cash_flow_statement = EmbeddedDocumentField(CashFlowStatement)
    comprehensive_income = EmbeddedDocumentField(ComprehensiveIncome)
    income_statement = EmbeddedDocumentField(IncomeStatement)


class PolygonFinancials(Document):
    filing_date = DateTimeField(required=True)
    ticker = StringField(required=True)
    fiscal_year = StringField()
    fiscal_period = StringField()
    company_name = StringField()
    source_filing_file_url = StringField()
    cik = StringField()
    source_filing_url = StringField()
    end_date_str = StringField(db_field="end_date")
    start_date_str = StringField(db_field="start_date")
    # Embed Financials as a field within PolygonFinancials
    financials = EmbeddedDocumentField(Financials)
    management_discussion = StringField()
    sentiment_score = FloatField(db_field="sentiment_score")
    id = StringField(db_field="_id")

    meta = {
        "db_alias": data_db_alias,
        "indexes": [
            {"fields": ["filing_date", "ticker"]},
        ]    
    }

    @classmethod
    def __get_pydantic_core_schema__(
        cls, source_type: Any, handler: GetCoreSchemaHandler
    ) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, handler(str))           