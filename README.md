## Introduction
This repository contains source code and content which was created in course of my master thesis project, which was named "LLM powered autonomous agents driven simulation of stock markets".

## Content of this repository
### Input data
The raw input data used in this thesis has not been uploaded to the Gitlab repository, as the author does not have the rights to distribute this data. Most of the data comes from APIs that require payment. The source code to load this data has not been disclosed. However, the metadata of the input data has been made accessible in the repository.

[Metadata of input data](/metadata/data_classes.py)

### Data produced during simulation
Data produced during the simulation, such as component scores, trader recommendations, trades, etc., has been fully made available in the form of a JSON file.

[Metadata of simulation data](/metadata/simulation_classes.py)

[Simulation data](/simulation_data/)

### Source code of the simulation framework
The program code to run the simulation has only been partially published, in accordance with the wishes of the thesis sponsor.
The hull of the simulation framework, where AutoGen chat get started lies in this file:

[Simulation framework with AutoGen](simulation_framework/run_simulation.py)

[Open AI model configuration](simulation_framework/OAI_CONFIG_LIST)

The source code of various functions, especially the "simulation" class is not disclosed.

### Jupyter notebooks
Various Jupyter notebooks have been published. These usually contain analysis and verifications which was done after running the simulation. Again, some of the underlying data or source code was not published.

[Recommendation components analysis](jupyter_notebooks/recommendation_components.ipynb)

[Recommendation components LSTM](/jupyter_notebooks/recommendation_components_lstm_model.ipynb)

[Portfolio manager analysis](jupyter_notebooks/portfolio_manager.ipynb)

## Abstract master thesis
### Introduction
Since the 1970s, the integration of computer science into trading has significantly evolved, from basic rule-based systems to the advanced application of machine learning and AI in algorithmic trading. Noteworthy among recent advancements are Large Language Models (LLMs), which have opened new possibilities for trading through their capability to analyze diverse data sources impacting the stock market. This thesis explores a novel approach using LLM-based autonomous agents for stock market simulations. These agents are autonomous, AI-powered programs driven by LLMs, designed to perform complex tasks by emulating human cognitive functions such as memory, planning, and decision-making. They can analyze vast amounts of data, interpret contextual information, and interact dynamically within their environments to achieve predefined goals. In this thesis, LLM agents emulate the roles of human traders and portfolio managers, processing various types of financial data to generate trading signals and execute trades in a simulated stock market environment. The aim is to evaluate whether these agents can develop effective and profitable investment strategies.
### Approach
This work introduces a novel simulation framework for the stock market where human traders are represented by autonomous LLM agents that analyze input data. The input data is categorized into seven components: Stock News, Prices and Volumes, Technical Analysis, Financials, General News, Economic Indicators, and Miscellaneous. The data for these components was preprocessed and is ultimately presented to the LLM traders in form of a natural language prompt. After analyzing the input data, the LLM traders generate recommendations indicating the type of position they want to hold for the next timestep (long, short, or no position). To do this, the LLM traders must use tools that were provided to them. These tools are esentially Python functions which the LLM agent use to interact with the simulation environment. Using a backtesting approach, traders receive feedback on their recommendations, enabling them to improve over time. 
Additionally, two types of portfolio managers were part of the simulation framework: one driven by LLMs and one based on a simple trading algorithm. Portfolio managers make decisions to buy, hold, or sell stocks. These decisions are based on the trader recommendations provided for the current timestep, as well as the past performance of every LLM trader. Each portfolio manager operates with an initially empty portfolio and a specified amount of starting capital, aiming to maximize profit.
### Result
During the model selection phase, experiments were conducted with various proprietary and open-source LLMs. Ultimately, OpenAI's most cost-efficient model, GPT-3.5 Turbo, was chosen as the backbone for the LLM agents. Although LLM agents are still in their infancy and face significant limitations - such as high inference times, elevated costs, limited context length, rate limits, hallucinations, and model-specific issues - this study demonstrates the feasibility of simulating the stock market using autonomous LLM agents. Reasoning traces revealed that LLM agents occasionally misjudged input data or made incorrect assumptions, likely due to the limitations of the model used. Despite these challenges, both types of portfolio managers achieved positive performance in the simulation, although it was below the benchmark.

## Simulation architecture
![Simulation architecture](images/simulation_architecture.png)

## Simulation steps
A single simulation day is structured into the following three sequential steps:
### 1. Collect recommendation component scores
This initial step involves gathering various scores that contribute to forming the overall trader recommendations. These scores can be interpreted as sub-scores that support the LLM reasoning process.

![Step 1: Collect recommendation component scores](images/1_collect_recommendation_component_scores.png)

### 2. Insert trader recommendations
Following the collection of component scores, these are consolidated into comprehensive trader recommendations. Each trader agent analyzes the data and submits their suggested trading actions for each ticker included in the simulation.

![Step 2: Insert trader recommendations](images/2_trader_recommendations.png)

### 3. Trading by Portfolio Managers
The portfolio managers execute trades based on the trader recommendations. This includes buying, holding, or selling stock positions as advised by the LLM trader agents in the previous step.

![Step 3: Trading by LLM Portfolio Manager](images/3_portfolio_managers.png)
